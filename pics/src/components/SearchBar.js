import React from "react";
import { useState } from "react";

export default function SearchBar({ onSubmit }) {
  const [term, setTerm] = useState("");

  const handleChange = (event) => {
    setTerm(event.target.value);
  };

  const handleFormSubmit = (event) => {
    event.preventDefault();
    onSubmit(term);
  };
  return (
    <div>
      <form onSubmit={handleFormSubmit}>
        <input className="input m-5 p-1" onChange={handleChange} value={term} />
      </form>
    </div>
  );
}
